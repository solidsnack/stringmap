
#ifndef cheddar_generated_stringmap_h
#define cheddar_generated_stringmap_h


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>



typedef struct stringmap_t stringmap_t;

stringmap_t* stringmap_new(size_t count, char const* const* keys, char const* const* values);

char const* const* stringmap_keys(stringmap_t const* m);

char const* stringmap_get(stringmap_t const* m, char const* key);

size_t stringmap_len(stringmap_t const* m);

void stringmap_free(stringmap_t* m);



#ifdef __cplusplus
}
#endif


#endif
