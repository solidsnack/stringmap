Provides an immutable map that can be shared between Rust, C, and languages
that bind to C in any combination. Python (using `cffi`) and Ruby (using
`ffi`) bindings are included.

Both the keys and values are returned as pointers to C strings, in memory
managed by the map. Callers do not need to free said pointers.

The Ruby (`bindings/stringmap.rb`) and Python (`bindings/stringmap.py`)
provide examples of usage. In the Python case, the map has been made to
conform to the standard mapping interface.
