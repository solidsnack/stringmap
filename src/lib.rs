use std::borrow::Borrow;
use std::collections::HashMap;
use std::ffi::{CStr, CString};
use std::str::from_utf8_unchecked;
use std::ptr;

extern crate libc;
use libc::c_char;

pub mod c;
pub use c::*;


/// Stores textual, read-only data in a way that is friendly to C as well as
/// Rust. The underlying strings can be passed directly to C, without copies,
/// since they are null-terminated; but this detail does not affect the Rust
/// interface.
pub struct StringMap {
    keys: Vec<*const c_char>,                    // To preserve insertion order
    data: HashMap<CString, CString>,
}

impl StringMap {
    pub fn new<'input, P, S, T>(tuples: T) -> StringMap
        where P: Borrow<(S, S)>,
              S: Borrow<str>,
              T: IntoIterator<Item = P>
    {
        let mut keys: Vec<*const c_char> = Vec::default();
        let mut data: HashMap<CString, CString> = HashMap::default();
        for pair in tuples {
            let &(ref k, ref v) = pair.borrow();
            if let Ok(k) = CString::new(k.borrow()) {
                if let Ok(v) = CString::new(v.borrow()) {
                    keys.push(k.as_ptr() as *const c_char);
                    data.insert(k, v);
                }
            }
        }
        keys.push(ptr::null());
        StringMap { keys: keys, data: data }
    }

    pub fn keys(&self) -> Vec<&str> {
        self.keys[0..(self.keys.len() - 1)]
            .iter()
            .map(|&p| unsafe { CStr::from_ptr(p) })
            .map(|s| unsafe { from_utf8_unchecked(s.to_bytes()) })
            .collect()
    }

    pub fn get(&self, key: &str) -> Option<&str> {
        if let Ok(ref k) = CString::new(key) {
            self.data
                .get(k)
                .map(|s| unsafe { from_utf8_unchecked(s.as_bytes()) })
        } else {
            None
        }
    }

    pub fn len(&self) -> usize { self.data.len() }
}


#[cfg(test)]
mod tests {
    use libc::c_char;

    use ::c::*;
    use ::StringMap;


    #[test]
    fn it_works() {
        let data = vec![("a", "A"), ("b", "B")];
        let m = StringMap::new(&data);
        assert!(m.len() == 2);
        assert!(m.keys() == vec!["a", "b"]);
        assert!(m.get("a") == Some("A"));
        assert!(m.get("b") == Some("B"));
    }


    #[test]
    fn extern_basics() {
        let data = vec![("a", "A"), ("b", "B")];
        let keys: Vec<*const c_char> =
            data.iter().map(|&(s, _)| s.as_ptr() as *const c_char).collect();
        let vals: Vec<*const c_char> =
            data.iter().map(|&(_, s)| s.as_ptr() as *const c_char).collect();
        unsafe {
            let m = stringmap_new(data.len(), keys.as_ptr(), vals.as_ptr());
            assert!(stringmap_len(m) == 2);
            assert!(!stringmap_keys(m).is_null());
            stringmap_free(m);
        }
    }
}
